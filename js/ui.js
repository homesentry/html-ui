var jsondata;
var imgid = [];
var imgurl = [];
var index;
var i=0;
var number;
var inurl;

function getData() 
{
	number = document.getElementById("number").value;
	inurl = "https://kmr0fi5yfd.execute-api.us-west-2.amazonaws.com/v1/label?number="+number;
	console.log(inurl);
	request = new XMLHttpRequest;
	request.open('GET', inurl, true);
	
	request.onload = function() {
	  if (request.status >= 200 && request.status < 400){

	  	jsondata = JSON.parse(request.responseText);
	  	imgid = Object.keys(jsondata);
		imgurl = Object.values(jsondata);
		console.log(imgurl)
		index=0;
		getNextImage();
	  }
	  else {
	    	console.log("error");
	  }
	};
	request.onerror = function() {
	};
	request.send();

}

function getNextImage()
{

	document.getElementById("imgno").innerHTML = "Image ("+(index+1)+"/"+imgid.length+")"
	document.getElementById("image").innerHTML = "<img src="+imgurl[index]+" id="+imgid[index]+"/>";
	
}
function goBack() {
	if (index >= (imgid.length)) {
		index = imgid.length-1;
	}
	if (index>0) {
		index = index-1;
		getNextImage();
		console.log(index)
	} 
	else {
		document.getElementById("imgno").innerHTML = "Already on the first image";
	}
}

function sendData()
{
	if(index < (imgid.length)) {
		index = index+1;
	}
	
	var jsonPOSTdata;
	var people = document.getElementById('people').value;
	var data = {
		"event_data":{
			"key": imgid[index-1],
			"num_people": people
		}
	};
	jsonPOSTdata = JSON.stringify(data);
	document.getElementById("data").innerHTML += (index-1)+"Previous Image: "+jsonPOSTdata+"<br>";
	outurl = "https://kmr0fi5yfd.execute-api.us-west-2.amazonaws.com/v1/select";
	var outrequest = new XMLHttpRequest();
	outrequest.open("POST",outurl,true);
	outrequest.setRequestHeader('Content-type','application/json; charset=utf-8');
	console.log(index);
	

	outrequest.send(jsonPOSTdata);

	document.getElementById('people').value = 0;
	
	if (index < imgid.length)
	{
		getNextImage()
	} 
	else {
		document.getElementById("end").innerHTML = "<h1>Current Sequence finished.</h1>"
	}
	
}